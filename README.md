# Introduction

This is a distribution of ScalaTrace 4, the next generation of the 
ScalaTrace tool that collects communication and I/O traces for parallel 
applications using the Message Passing Interface.

It is based on the official Scalatrace distribution, with added patches and scripts

# Compiling Scalatrace
 
Just run `make` from the top directory.

# Generating a trace

```
$ source setenv.sh
$ echo $LDFLAGS 
-L/home/trahay/Soft/opt/scalatrace/record/lib -L/home/trahay/Soft/opt/scalatrace/stackwalk/ver0 -lglob -lstackwalk -lm -lstdc++
$ mpicc mpi_ping.c   -o mpi_ping $LDFLAGS
$ mpirun -np 2 ./mpi_ping
(olaf): My rank is 0
(olaf): My rank is 1
10000   16      13.248300
time=(0.282903)
[olaf:134901] *** Process received signal ***
[olaf:134901] Signal: Segmentation fault (11)
[olaf:134901] Signal code: Address not mapped (1)
[olaf:134901] Failing at address: 0x55fe6ef50f01
[olaf:134901] [ 0] /lib/x86_64-linux-gnu/libc.so.6(+0x3c510)[0x7fe21a65a510]
[olaf:134901] [ 1] ./mpi_ping(+0x571c5)[0x55fb3027e1c5]
[olaf:134901] [ 2] ./mpi_ping(+0x459b8)[0x55fb3026c9b8]
[olaf:134901] [ 3] ./mpi_ping(+0x68fdc)[0x55fb3028ffdc]
[olaf:134901] [ 4] /lib/x86_64-linux-gnu/libc.so.6(+0x3e955)[0x7fe21a65c955]
[olaf:134901] [ 5] /lib/x86_64-linux-gnu/libc.so.6(+0x3ea8a)[0x7fe21a65ca8a]
[olaf:134901] [ 6] /lib/x86_64-linux-gnu/libc.so.6(+0x276d1)[0x7fe21a6456d1]
[olaf:134901] [ 7] /lib/x86_64-linux-gnu/libc.so.6(__libc_start_main+0x85)[0x7fe21a645785]
[olaf:134901] [ 8] ./mpi_ping(+0x121e1)[0x55fb302391e1]
[olaf:134901] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 0 with PID 0 on node olaf exited on signal 11 (Segmentation fault).
--------------------------------------------------------------------------
```

The segmentation fault is not a problem because it happens after the trace was written to disk (it would be nice to fix the problem though).

# Reading a trace

`scalatrace_print` is a tentative program that prints the content of a trace:
```
$ scalatrace_print trace_2/0
[...]
[30512] MPI_Send(dest=0, tag=0, len=16)
[30513] MPI_Recv(src=0, tag=0, len=16)
[30515] MPI_Send(dest=0, tag=0, len=16)
[30516] MPI_Recv(src=0, tag=0, len=16)
[30518] MPI_Send(dest=0, tag=0, len=16)
[30519] MPI_Recv(src=0, tag=0, len=16)
[30521] MPI_Send(dest=0, tag=0, len=16)
[30522] MPI_Recv(src=0, tag=0, len=16)
[...]
```


# Documentation

See `README` for the offical Scalatrace documentation
