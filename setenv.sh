#!/bin/bash

export SCALATRACE_ROOT=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export PATH=$PATH:$SCALATRACE_ROOT/replay:$SCALATRACE_ROOT/scalatrace_print
export LDFLAGS="-L${SCALATRACE_ROOT}/record/lib -L${SCALATRACE_ROOT}/stackwalk/ver0 -lglob -lstackwalk -lm -lstdc++ $LDFLAGS"
