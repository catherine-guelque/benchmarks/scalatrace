
LIBS=record/lib/libglob.a record/lib/libdump.a record/lib/libnode.a stackwalk/ver0/libstackwalk.a replay/replay scalatrace_print/scalatrace_print
all: $(LIBS)

record/lib/libglob.a:
	make -C record
record/lib/libdump.a:
	make -C record
record/lib/libnode.a:
	make -C record
scalatrace_print/scalatrace_print: record/lib/libglob.a record/lib/libdump.a record/lib/libnode.a
	make -C scalatrace_print

stackwalk/ver0/libstackwalk.a:
	make -C stackwalk/ver0
replay/replay: record/lib/libglob.a record/lib/libdump.a record/lib/libnode.a
	make -C replay

clean:
	make -C stackwalk/ver0 clean
	make -C record clean
	make -C replay clean
	make -C scalatrace_print clean
